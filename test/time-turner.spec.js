delete Array.prototype.reverse;
var expect = require('expect.js');
var timeTurner = require('../sources/time-turner.js');

Array.prototype.reverse = timeTurner;

suite('Must return an array', function(){

    test('1 point for Gryffindor!', function(){
        var array = [1, 2, 3, 4, 5];
        var result = array.reverse();
        expect(result).to.be.an('array');
    });

});

suite('Must return a reversed array', function(){

    test('10 points for Gryffindor!', function(){
        var array = ["a","b","c","d"];
        var array2 = [2,"b",10,"d", "dfg", 123];
        var result = array.reverse();
        var result2 = array2.reverse();
        expect(result).to.eql(["d","c","b","a"]);
        expect(result2).to.eql([123,"dfg","d",10, "b", 2]);
    });

});

suite('Must return the original array', function(){

    test('100 points for Gryffindor!', function(){
        var array = [1, 2, 3, 4, 5];
        var result = array.reverse();
        expect(result).to.equal(array);
    });

});
