# Time-turner

Dumbledore works on a new Time-turner but he doesn't find his blueprints...

You have to help him to create the new Time-turner and earn points for Gryffindor!


Array.prototype.reverse doesn't exist. You have to recreate the method.

example:

`
var array = [1,2,3,4];

array.reverse() // array.reverse is not a function
`

Good luck Gryffindor!


SPECS

Must return an array
1 point for Gryffindor!

Must return a reversed array
10 points for Gryffindor!

Must return the original array
100 points for Gryffindor!