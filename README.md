# Time-turner

Dumbledore is creating a new Time-turner, but can’t find his blueprints...
You have to help him create a new Time-turner and earn points for Gryffindor!


The function Array.prototype.reverse doesn't exist. You have to recreate the method.

example:

`
var array = [1,2,3,4];
array.reverse() // array.reverse is not a function
`

Good luck, Gryffindor!
